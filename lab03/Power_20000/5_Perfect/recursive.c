#include <stdio.h>

#define BASE 2
#define EXPN 20000

double recursive(double base, int expn) {
	if (expn == 0)
		return 1;
	double sq = recursive(base, expn / 2);
	double ans = sq * sq;
	if (expn % 2 == 1)
		ans = ans * base;
	return ans;
}

int main(void) {
	double ans = recursive(BASE, EXPN);
//	printf("%f\n", ans);
	return 0;
}
