#include <stdio.h>

#define BASE 2
#define EXPN 20000

double iterative(double base, int expn) {
	double ans = 1;
	int i;
	for (i = 0; i < expn; i++) {
		ans = ans * base;
	}
	return ans;
}

int main(void) {
	double ans = iterative(BASE, EXPN);
//	printf("%f\n", ans);
	return 0;
}
