/* adopted in part from http://www.cs.northwestern.edu/academics/courses/110/ */

#include <stdio.h>
#define NUMBER 16

long fibonacci(long);
long fib2(unsigned int n, unsigned long p0, unsigned long p1);

main() {

 long i;
 i = fibonacci(NUMBER);

}

long fibonacci(long n) {

  return n == 0 ? 0 : fib2(n, 0, 1);

}
  
long fib2(unsigned int n, unsigned long p0, unsigned long p1) {		

	return n == 1 ? p1 : fib2(n - 1, p1, p0 + p1);

}

