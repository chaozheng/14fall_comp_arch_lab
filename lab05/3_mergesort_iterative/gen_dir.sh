#!/bin/sh

for i in 1 2 3 4 5 6 7 8
do  
    if [ -d "config$i" ]
    then
	echo "config$i exists"
    else
        cp -r baseline config$i
    fi
done
